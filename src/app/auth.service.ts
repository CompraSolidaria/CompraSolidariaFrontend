import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';
import { myConfig } from './auth.config';

// Avoid name not found warnings
declare var auth0: any;

@Injectable()
export class AuthService {

  public userProfile: Object;
  public isAutenthicating: boolean;

  // Configure Auth0
  private auth0 = new auth0.WebAuth({
    domain: myConfig.domain,
    clientID: myConfig.clientID,
    redirectUri: myConfig.callbackURL,
    responseType: 'token id_token'
  });

  constructor(private router: Router) {
    if (localStorage.getItem('profile')) {
      this.userProfile = JSON.parse(localStorage.getItem('profile'));
    }
  }

  public handleAuthentication(): void {
    this.auth0.parseHash({ _idTokenVerification: false }, (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setUser(authResult);
        this.auth0.client.userInfo(authResult.accessToken, (err, user) => {
          if (user) {
            localStorage.setItem('profile', JSON.stringify(user));
            this.userProfile = user;
          } else if (err) {
            console.log(err);
          }
        });
        this.router.navigate(['/']);
        this.isAutenthicating = false;
      } else if (err) {
        console.log(err);
        this.isAutenthicating = false;
      }
    });
  }

  public login(username: string, password: string): void {
    this.isAutenthicating = true;
    this.auth0.redirect.loginWithCredentials({
      connection: 'Username-Password-Authentication',
      username,
      password
    }, err => {
      this.isAutenthicating = false;
      if (err) return alert(err.description);
    });
  }

  public isAuthenticated(): boolean {
    // Check whether the id_token is expired or not
    return tokenNotExpired();
  }

  public logout(): void {
    // Remove token from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    if (this.userProfile != undefined) {
      this.auth0.logout({
        returnTo: myConfig.logoutURL,
        client_id: this.userProfile['clientID']
      });
      this.userProfile = undefined;
    }
    this.isAutenthicating = false;
  }

  private setUser(authResult): void {
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
  }

  public getIdToken(): string {
    return localStorage.getItem('id_token').toString();
  }
}
