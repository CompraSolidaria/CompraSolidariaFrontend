import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Campaign } from './campaign';
import { Picture } from './picture';
import { AuthService } from './auth.service';

@Injectable()
export class CampaignService {

  private baseUrl: string = 'http://localhost:3000/campaigns';

  public selectedCampaign: Campaign;

  constructor(private http: Http, private auth: AuthService) { }

  public findNewId(): Observable<string> {
    return this.http.get(this.baseUrl + '/free').map((resp: Response) => {
      return resp.json() as string;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public searchCampaigns(field: string, value: string): Observable<Campaign[]> {
    return this.http.get(this.baseUrl + `/search/all/${field}/${value}`).map((resp: Response) => {
      return resp.json() as Campaign[];
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public saveCampaign(campaign: Campaign): Observable<boolean> {
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.post(this.baseUrl + '/', JSON.stringify(campaign), options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public patchCampaign(campaignId: string, field: string, fieldValue): Observable<boolean> {
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.patch(this.baseUrl + `/${campaignId}/${field}`, fieldValue, options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public deleteCampaign(campaignId: string): Observable<boolean> {
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.delete(this.baseUrl + `/${campaignId}`, options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public getCampaignPictures(campaignId: string): Observable<Picture[]> {
    return this.http.get(this.baseUrl + `/${campaignId}/pictures`).map((resp: Response) => {
      return resp.json() as Picture[];
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public saveCampaignPicture(campaignId: string, picture: Picture): Observable<boolean> {
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.post(this.baseUrl + `/${campaignId}/pictures`, JSON.stringify(picture), options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public patchCampaignPicture(campaignId: string, picture: Picture, oldUrl: string): Observable<boolean> {
    var updatePic = JSON.parse(JSON.stringify(picture));
    updatePic.oldUrl = oldUrl;
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.patch(this.baseUrl + `/${campaignId}/pictures`, JSON.stringify(updatePic), options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }

  public deleteCampaignPicture(campaignId: string, picUrl: string): Observable<boolean> {
    var mHeaders = new Headers;
    mHeaders.append('Authorization', 'Bearer ' + this.auth.getIdToken());
    mHeaders.append('Content-Type', 'application/json');
    var options = new RequestOptions();
    options.headers = mHeaders;
    return this.http.delete(this.baseUrl + `/${campaignId}/pictures/${picUrl}`, options).map((resp: Response) => {
      return (resp.status === 200) as boolean;
    }).catch((err) => {
      return Observable.throw(err);
    });
  }
}
