import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AuthService } from './auth.service';
import { CampaignService } from './campaign.service';
import { routing, appRoutingProviders } from './app.routes';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CampaignSearchComponent, DialogCampaignDelete } from './campaign-search/campaign-search.component';
import { CampaignRegisterComponent } from './campaign-register/campaign-register.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    HomeComponent,
    CampaignSearchComponent,
    DialogCampaignDelete,
    CampaignRegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    routing
  ],
  providers: [
    AuthService,
    CampaignService,
    appRoutingProviders
  ],
  entryComponents: [
    DialogCampaignDelete
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
