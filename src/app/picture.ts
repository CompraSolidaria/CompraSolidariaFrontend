export class Picture {
    public banner: string;
    public url: string;

    constructor(banner: string, url: string) {
        this.banner = banner;
        this.url = url;
    }
}
