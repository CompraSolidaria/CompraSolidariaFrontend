import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CampaignService } from '../campaign.service';
import { Campaign } from '../campaign';
import { Picture } from '../picture';

@Component({
  selector: 'app-campaign-search',
  templateUrl: './campaign-search.component.html',
  styleUrls: ['./campaign-search.component.css']
})
export class CampaignSearchComponent implements OnInit {

  disableButton: boolean[] = [];
  campaigns: Campaign[] = [];
  searching: boolean = false;
  subscription: Subscription;

  fields = [
    { value: 'id', viewValue: 'ID' },
    { value: 'name', viewValue: 'Nome' },
    { value: 'status', viewValue: 'Status' }
  ];

  public campaignSearchForm = new FormGroup({
    searchField: new FormControl(null, Validators.required),
    searchValue: new FormControl(null, Validators.required)
  });

  constructor(private snackBar: MdSnackBar, private dialog: MdDialog, private router: Router, private campaignService: CampaignService) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private clearSearch() {
    this.campaignSearchForm.reset();
    this.campaigns = [];
  }

  private retrieveCampaigns(field: string, value: string) {
    this.searching = true;
    this.subscription = this.campaignService.searchCampaigns(field, value)
      .flatMap((campaigns) => {
        if (campaigns.length > 0) {
          return campaigns;
        } else {
          this.subscription.unsubscribe;
          this.searching = false;
          return Observable.throw('Nenhuma campanha encontrada');
        }
      })
      .map((campaign: Campaign) => {
        var arrCampaign = new Campaign();
        arrCampaign.id = campaign.id;
        arrCampaign.name = campaign.name;
        arrCampaign.status = campaign.status;
        arrCampaign.votes = campaign.votes;
        arrCampaign.description = campaign.description;
        this.campaigns.push(arrCampaign);

        return campaign;
      })
      .map((currCampaign: Campaign) => {
        return this.campaignService.getCampaignPictures(currCampaign.id)
          .map((pictures) => {
            for (let i = 0; i < pictures.length; i++) {
              var arrPicture = new Picture(pictures[i].banner, pictures[i].url);
              this.campaigns.find((campaign) => campaign.id == currCampaign.id).pictures.push(arrPicture);
            }
          });
      })
      .flatMap((pictures) => pictures)
      .subscribe(() => { }, (err) => {
        this.searching = false;
        if (err == 'Nenhuma campanha encontrada') {
          let snackBarRef = this.snackBar.open(err, 'Fechar', { duration: 3000 });
        } else {
          console.log(err);
          let snackBarRef = this.snackBar.open('Erro ao buscar campanhas', 'Fechar', { duration: 3000 });
        }
      }, () => { this.searching = false; });
  }

  private deleteCampaign(campaign: Campaign) {
    this.disableButton[campaign.id] = true;
    let dialogRef = this.dialog.open(DialogCampaignDelete);
    dialogRef.componentInstance.message = `Confirmar remoção da campanha ${campaign.id}?`;
    dialogRef.afterClosed().subscribe(option => {
      if (option === true) {
        this.subscription = this.campaignService.deleteCampaign(campaign.id).subscribe((deleteOK) => {
          if (deleteOK === true) {
            this.campaigns.splice(this.campaigns.indexOf(campaign), 1);
            let snackBarRef = this.snackBar.open(`Campanha ${campaign.id} removida`, 'Fechar', { duration: 3000 });
          }
        }, (err) => {
          console.log(err);
          let snackBarRef = this.snackBar.open(`Erro ao remover campanha ${campaign.id}`, 'Fechar', { duration: 3000 });
          this.disableButton[campaign.id] = false;
        });
      } else {
        this.disableButton[campaign.id] = false;
      }
    });
  }

  private editCampaign(campaign: Campaign) {
    this.campaignService.selectedCampaign = campaign;
    this.router.navigate(['/campaigns/new']);
  }
}

@Component({
  selector: 'dialog-campaign-delete',
  template: `
  <h1 md-dialog-title>Remover campanha</h1>
  <div md-dialog-content>{{ message }}</div>
  <div md-dialog-actions>
    <button md-button (click)="dialogRef.close(false)">Cancelar</button>
    <button md-button color="warn" (click)="dialogRef.close(true)">Confirmar</button>
  </div>
  `
})
export class DialogCampaignDelete {

  public message: string;

  constructor(public dialogRef: MdDialogRef<DialogCampaignDelete>) { }
}