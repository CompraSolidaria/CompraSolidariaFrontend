import { Picture } from './picture';

export class Campaign {
    public id: string;
    public name: string;
    public description: string;
    public status: string;
    public votes: number;
    public pictures: Picture[] = [];
}
