interface AuthConfiguration {
    clientID: string,
    domain: string,
    callbackURL: string,
    logoutURL: string
}

export const myConfig: AuthConfiguration = {
    clientID: 'lGk2yq9D371IVbeNX0moaF0OLVJPRdFi',
    domain: 'lvinagre.auth0.com',
    callbackURL: 'http://localhost:4200/home',
    logoutURL: 'http://localhost:4200'
};
