import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { CampaignService } from '../campaign.service';
import { Campaign } from '../campaign';
import { Picture } from '../picture';

@Component({
  selector: 'app-campaign-register',
  templateUrl: './campaign-register.component.html',
  styleUrls: ['./campaign-register.component.css']
})
export class CampaignRegisterComponent implements OnInit {

  campaign: Campaign;
  ready: boolean = false;
  registering: boolean = false;
  saved: boolean = false;
  subscription: Subscription;
  submitBtnLabel: String;

  fields = [
    { value: 'V', viewValue: 'Votável' },
    { value: 'R', viewValue: 'Resultados' },
  ];

  campaignRegisterForm = new FormGroup({
    id: new FormControl({ value: '', disabled: true }, Validators.required),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
    pictures: new FormArray([], Validators.required)
  });

  constructor(private snackBar: MdSnackBar, private router: Router, private campaignService: CampaignService) {
  }

  ngOnInit() {
    if (!this.campaignService.selectedCampaign) {
      this.newCampaign();
    } else {
      this.campaign = Object.assign(new Campaign(), this.campaignService.selectedCampaign);
      this.campaign.pictures = JSON.parse(JSON.stringify(this.campaignService.selectedCampaign.pictures));
      var pictures = this.campaignRegisterForm.controls['pictures'] as FormArray;
      for (let i = 0; i < this.campaign.pictures.length; i++) {
        pictures.push(new FormControl('', Validators.required));
      }
      this.submitBtnLabel = 'Atualizar Campanha';
      this.ready = true;
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.campaignService.selectedCampaign) {
      this.campaignService.selectedCampaign = null;
    }
  }

  public submitCampaign(campaign: Campaign) {
    if (this.submitBtnLabel == 'Registrar Campanha') {
      this.saveCampaign(campaign);
    } else {
      this.updateCampaign(campaign);
    }
  }

  public saveCampaign(campaign: Campaign) {
    this.registering = true;
    this.subscription = this.campaignService.saveCampaign(campaign).subscribe((saveOK) => {
      if (saveOK === true) {
        var pictureError: boolean = false;

        for (let i = 0; i < campaign.pictures.length; i++) {
          if (!pictureError) {
            this.subscription = this.campaignService.saveCampaignPicture(campaign.id, campaign.pictures[i]).subscribe((saveOK) => {
              if (saveOK === true && i == campaign.pictures.length - 1) {
                let snackBarRef = this.snackBar.open(`Campanha ${campaign.id} cadastrada`, 'Fechar', { duration: 3000 });
                this.registering = false;
                this.saved = true;
              }
            }, (err) => {
              pictureError = true;
              this.subscription = this.campaignService.deleteCampaign(campaign.id).subscribe((deleteOK) => {
                if (deleteOK === true) {
                  let snackBarRef = this.snackBar.open(`Erro ao cadastrar campanha ${campaign.id} (dados removidos)`, 'Fechar', { duration: 3000 });
                  this.registering = false;
                }
              }, (err) => {
                console.log(err);
                let snackBarRef = this.snackBar.open(`Erro ao cadastrar campanha ${campaign.id} (dados inconsistentes)`, 'Fechar', { duration: 3000 });
                this.registering = false;
                this.saved = true;
              });
            });
          } else {
            break;
          }
        }
      }
    }, (err) => {
      console.log(err);
      let snackBarRef = this.snackBar.open(`Erro ao cadastrar campanha ${campaign.id}`, 'Fechar', { duration: 3000 });
      this.registering = false;
    });
  }

  public updateCampaign(campaign: Campaign) {
    var updateControl = false;
    this.registering = true;

    if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
      let snackBarRef = this.snackBar.open(`Não houve alteração na campanha`, 'Fechar', { duration: 3000 });
      updateControl = true;
      this.registering = false;
    } else {

      if (JSON.stringify(campaign.pictures) !== JSON.stringify(this.campaignService.selectedCampaign.pictures) && !updateControl) {
        this.campaignService.selectedCampaign.pictures.forEach(oldPicture => {
          this.subscription = this.campaignService.deleteCampaignPicture(campaign.id, encodeURIComponent(oldPicture.url).replace(/'/g, "%27").replace(/"/g, "%22")).subscribe((saveOK) => {
            if (saveOK === true) {
              this.campaignService.selectedCampaign.pictures.shift();

              if (this.campaignService.selectedCampaign.pictures.length === 0) {
                this.campaign.pictures.forEach(picture => {
                  this.subscription = this.campaignService.saveCampaignPicture(campaign.id, picture).subscribe((saveOK) => {
                    if (saveOK === true) {
                      this.campaignService.selectedCampaign.pictures.push(picture);

                      if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
                        updateControl = true;
                        this.registering = false;
                        this.saved = true;
                      }
                    }
                  }, (err) => {
                    let snackBarRef = this.snackBar.open(`Erro ao atualizar dados da campanha`, 'Fechar', { duration: 3000 });
                    updateControl = true;
                    this.registering = false;
                  });
                });

                if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
                  updateControl = true;
                  this.registering = false;
                  this.saved = true;
                }
              }
            }
          }, (err) => {
            let snackBarRef = this.snackBar.open(`Erro ao atualizar dados da campanha`, 'Fechar', { duration: 3000 });
            updateControl = true;
            this.registering = false;
          });
        });
      }

      if (campaign.name !== this.campaignService.selectedCampaign.name && !updateControl) {
        this.subscription = this.campaignService.patchCampaign(campaign.id, "name", { "name": campaign.name }).subscribe((saveOK) => {
          if (saveOK === true) {
            this.campaignService.selectedCampaign.name = campaign.name;

            if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
              updateControl = true;
              this.registering = false;
              this.saved = true;
            }
          }
        }, (err) => {
          let snackBarRef = this.snackBar.open(`Erro ao atualizar dados da campanha`, 'Fechar', { duration: 3000 });
          updateControl = true;
          this.registering = false;
        });
      }

      if (campaign.status !== this.campaignService.selectedCampaign.status && !updateControl) {
        this.subscription = this.campaignService.patchCampaign(campaign.id, "status", { "status": campaign.status }).subscribe((saveOK) => {
          if (saveOK === true) {
            this.campaignService.selectedCampaign.status = campaign.status;

            if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
              updateControl = true;
              this.registering = false;
              this.saved = true;
            }
          }
        }, (err) => {
          let snackBarRef = this.snackBar.open(`Erro ao atualizar dados da campanha`, 'Fechar', { duration: 3000 });
          updateControl = true;
          this.registering = false;
        });
      }

      if (campaign.description !== this.campaignService.selectedCampaign.description && !updateControl) {
        this.subscription = this.campaignService.patchCampaign(campaign.id, "description", { "description": campaign.description }).subscribe((saveOK) => {
          if (saveOK === true) {
            this.campaignService.selectedCampaign.description = campaign.description;

            if (JSON.stringify(campaign) === JSON.stringify(this.campaignService.selectedCampaign)) {
              updateControl = true;
              this.registering = false;
              this.saved = true;
            }
          }
        }, (err) => {
          let snackBarRef = this.snackBar.open(`Erro ao atualizar dados da campanha`, 'Fechar', { duration: 3000 });
          updateControl = true;
          this.registering = false;
        });
      }
    }
  }

  public newCampaign() {
    this.subscription = this.campaignService.findNewId().subscribe((newId) => {
      this.campaign = new Campaign;
      this.campaign.id = newId;
      var pictures = this.campaignRegisterForm.controls['pictures'] as FormArray;
      pictures.push(new FormControl('', Validators.required));
      this.campaign.pictures.push(new Picture("1", ""));
      this.submitBtnLabel = 'Registrar Campanha';
      this.ready = true;
    }, (err) => {
      console.log(err);
      let snackBarRef = this.snackBar.open('Impossível cadastrar campanha no momento', 'Fechar', { duration: 3000 });
      this.router.navigate(['/home']);
    });
  }

  public refreshCampaign() {
    this.resetState();
    this.newCampaign();
  }

  public addPicture(campaign: Campaign) {
    var pictures = this.campaignRegisterForm.controls['pictures'] as FormArray;
    pictures.push(new FormControl('', Validators.required));
    campaign.pictures.push(new Picture("0", ''));
  }

  public removePicture(campaign: Campaign, picture: Picture) {
    var pictures = this.campaignRegisterForm.controls['pictures'] as FormArray;
    pictures.removeAt(campaign.pictures.indexOf(picture));
    pictures.updateValueAndValidity();
    this.campaignRegisterForm.updateValueAndValidity();
    campaign.pictures.splice(campaign.pictures.indexOf(picture), 1);
  }

  private resetState() {
    this.campaign = null;
    var pictures = this.campaignRegisterForm.controls['pictures'] as FormArray;
    while (pictures.length > 0) {
      pictures.removeAt(0);
    }
    this.campaignRegisterForm.reset();
    this.campaignRegisterForm.updateValueAndValidity();
    this.ready = false;
    this.registering = false;
    this.saved = false;
  }
}
