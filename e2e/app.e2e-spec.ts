import { CompraSolidariaFrontendPage } from './app.po';

describe('compra-solidaria-frontend App', () => {
  let page: CompraSolidariaFrontendPage;

  beforeEach(() => {
    page = new CompraSolidariaFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
